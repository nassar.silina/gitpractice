package GitPractice;

public class CDFactory {
    private int noOfCDCreated;

    public CDFactory() {
        noOfCDCreated=0;
    }

    public Movies movieCreated(String title,double time){
        noOfCDCreated++;
        return new Movies(noOfCDCreated,title,time);
    }

    public Series seriesCreated(String title,int number){
        noOfCDCreated++;
        return new Series(noOfCDCreated,title,number);
    }
}
