package GitPractice;

public class CD {
    protected int id;
    protected String title;

    public CD(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
