package GitPractice;

import java.util.ArrayList;
import java.util.Scanner;

public class Controller {
    private ArrayList<CD> cds;
    private CDFactory cdFactory;
    private ArrayList<CD> availableCD;

    public Controller() {
        cds=new ArrayList<>();
        cdFactory=new CDFactory();
        availableCD=new ArrayList<>();

    }

    public void start(){
        Scanner scanner=new Scanner(System.in);
        System.out.println();
        System.out.println("Welcome to our Store");
        System.out.println();
        boolean stay=true;
        do {
            System.out.println("#Main options:-  0:Quit | 1:ADD Movie | 2:ADD Series | 3:Print available CDs | 4:Borrow a CD | 5:return CD ");
            System.out.print("Chosen Option:- ");
            int option = scanner.nextInt();
            scanner.nextLine();
            switch (option) {
                case 0:stay = false; break;
                case 1: addMovie(scanner); break;
                case 2: addSeries(scanner); break;
                case 3: printAvailableCd(); break;
                case 4: borrowCD(scanner); break;
                case 5: returnCD(scanner); break;
                default:
                    System.out.println("Not valid option");
            }
        }while (stay);

    }

    private void returnCD(Scanner scanner){
        System.out.println("#Please enter the name of the returned item");
        String name=scanner.nextLine();
        if (checkUsedName(name)!= null){
            System.out.println("Thank you for returning the CD");
        }else{
            System.out.println("The item is not exist in our list!");
        }

    }


    private void borrowCD(Scanner scanner){
        if (cds.isEmpty() || availableCD == null){
            System.out.println("Please add movies list and series list first");
        }else {
            System.out.println("This is our movie/series available list");
            printAvailableCd();
            System.out.println();
            System.out.println("#Please enter CD's name which you would like to borrow?");
            String name=scanner.nextLine();
            if (checkUsedName(name) !=null){
                System.out.println("The procedure was done");
                availableCD.remove(checkUsedName(name));

            }else {
                System.out.println("the item is not found");
            }

        }
    }

    private void printAvailableCd(){
        if (! availableCD.isEmpty()){
            for (CD cd: availableCD){
                System.out.println(cd);
            }
        }
    }

    private void addSeries(Scanner scanner) {
        System.out.println("#Enter the name of the series ");
        String name=scanner.nextLine();
        if (checkUsedName(name) == null){
            System.out.println("#Enter the number of its episodes");
            int number=scanner.nextInt();
            scanner.nextLine();
            Series seriesCreated=cdFactory.seriesCreated(name,number);
            cds.add(seriesCreated);
            availableCD.add(seriesCreated);
        }else {
            System.out.println("The item is added already");
        }

    }

    private void addMovie(Scanner scanner){
        System.out.println("#Enter the name of the movie ");
        String title = scanner.nextLine();
        if (checkUsedName(title)==null){
            System.out.println("#Enter the movie time duration in minutes ");
            double time =scanner.nextDouble();
            Movies movieCreated=cdFactory.movieCreated(title,time);
            cds.add(movieCreated);
            availableCD.add(movieCreated);
        }else {
            System.out.println("The item is added already");
        }
    }

    private CD checkUsedName(String name){
        for (CD cd: cds)
            if (cd.getTitle().equals(name)){
                return cd;
            }return null;
    }


}
