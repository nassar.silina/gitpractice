package GitPractice;

public class Movies extends CD{
    private double durationMin;

    public Movies(int id, String title, double durationMin) {
        super(id, title);
        this.durationMin = durationMin;

    }

    @Override
    public String toString() {
        return "Movies{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", durationMin=" + durationMin +
                '}';
    }
}
