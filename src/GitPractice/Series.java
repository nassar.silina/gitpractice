package GitPractice;

public class Series extends CD{
    private int noOfEpisodes;

    public Series(int id, String title, int noOfEpisodes) {
        super(id, title);
        this.noOfEpisodes = noOfEpisodes;
    }

    @Override
    public String toString() {
        return "Series{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", noOfEpisodes=" + noOfEpisodes +
                '}';
    }
}
